//
//  AppDelegate.h
//  sampleGitlabTest
//
//  Created by Ascra on 8/22/15.
//  Copyright (c) 2015 Ascra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

